#!/bin/bash
echo "--------------Меню BITRIX DCOKER-------------"
echo "1: Запустить контейнер"
echo "2: Остановить контейнер"
echo "3: Добавить сайт"
echo "4: Закрыть"
echo "Выберите действие:"
read action

case $action in
	1)
		docker-compose up -d
		;;
	2)
		docker-compose kill &&
		docker-compose down
		;;
	3)
		addsite="yes";

		echo "Домен (без www):"
		read domain
		echo "Папка с сайтом (путь относительно APP_PATH_HOST из настроек .env)"
		echo "По умолчанию название папки с проектом соответствует домену"
		read path

		if [ -z "$path" ]; then
			path="$domain";
		fi

		if test -f "web/vhosts/$domain.conf"; then
			echo "Сайт уже существует, пересоздать? (Y/N)"
			read action3

			case $action3 in
				"N")
					echo "Создание отменено"
					addsite="no";
					;;
				"Y")
					echo "Удаление сайта..."
					rm web/vhosts/$domain.conf
					;;
				*)
					echo "Отмена..."
					addsite="no";
					;;
			esac	
		fi

		if [ "$addsite" == "yes" ]; then
			echo "Добавление сайта..."

			vhost="<VirtualHost *:80>
    ServerAdmin admin@test.com
    ServerName $domain
	ServerAlias www.$domain
	DocumentRoot /var/www/html/$path
	#ErrorLog /var/www/html/$path/apache_logs/error.log
	#CustomLog /var/www/html/$path/apache_logs/access.log combined

	<Directory /var/www/html/$path>
		#Включение работы символических ссылок
		Options FollowSymLinks
		#Разрешение на перезапись всех директив при помощи .htaccess
		AllowOverride All
	</Directory>
</VirtualHost>";

			echo "$vhost" > web/vhosts/$domain.conf

			echo "Сборка контейнера..."
			docker-compose build > /dev/null
			echo "Контейнер собран"

			if grep -q "$domain" /etc/hosts; then
				echo "Запись в hosts уже существует"
			else
				echo "Добавление записи в hosts (нужны root права)"
				echo "127.0.0.1 $domain" | sudo tee --append /etc/hosts > /dev/null
				echo "Запись в hosts добавлена"
			fi
			
			echo "Сайт успешно добавлен"
			echo "1: Запустить контейнер"
			echo "0: Назад"
			echo "Выберите действие:"
			read action3
			
			case $action3 in
				1)
					docker-compose up -d
					;;
				0)
					;;
			esac
		else
			echo "Назад к меню..."
		fi
		;;
	4)
		exit 0
		;;
esac

./menu.sh
